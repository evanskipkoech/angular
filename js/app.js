angular.module('SoundPrint', ['ngAnimate','ui.router'])
	.config(function($stateProvider, $urlRouterProvider) {

		$urlRouterProvider.otherwise('/');
		
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'home.html'
			})
			.state('about', {
				url: '/about',
				templateUrl: 'about.html'
			});
	})
	.controller('SoundPrintCtrl', function(){

		var dirList = this;
		var toogle = true;

		dirList.list = [
			{name: 'Peter', age: 30, img: 'https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg'}, 
			{name: 'Chebet', age: 29, img: 'https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg'}, 
			{name: 'Ben', age: 35, img: 'https://s3.amazonaws.com/uifaces/faces/twitter/towhidzaman/128.jpg'}, 
			{name: 'Pheobe', age: 23, img: 'https://s3.amazonaws.com/uifaces/faces/twitter/ritu/128.jpg'}
		];

		dirList.addPerson = function(){
			dirList.list.push({name:dirList.name, age: dirList.age});
			dirList.name = '';
			dirList.age ='';
		};
		
	});